# README #

### Uruchomienie aplikacji ###
`./gradlew bootRun`

### Uruchomienie testów ###
`./gradlew test`

### Endpointy ###

#### Tłumaczenia

`POST /translate?quoted=1`

Przykładowe zapytanie: 
```
curl --location --request POST 'localhost:8080/translate' \
--header 'Content-Type: application/json' \
--data-raw '{
    "language": "pleng",
    "content": "jestem lotosu kwiatem"
}'
```
#### Statystyki słownika
`GET /stats/pleng`

Przykładowe zapytanie: 
```
curl --location --request GET 'http://localhost:8080/stats/pleng'
```