package com.example.translationservice.stats;

import com.example.translationservice.Language;
import com.example.translationservice.dictionary.LanguageDictionary;
import com.example.translationservice.dictionary.LanguageDictionaryWithStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

@Service
public class StatsService {

    private final Map<Language, LanguageDictionaryWithStatistics> dictionaries;

    @Autowired
    public StatsService(List<LanguageDictionaryWithStatistics> dictionaries) {
        this.dictionaries = dictionaries.stream().collect(toMap(LanguageDictionary::getLanguage, Function.identity()));
    }

    public Stats getKeysStats(Language language) {
        return dictionaries.get(language).getStats();
    }
}
