package com.example.translationservice.stats;

import com.example.translationservice.dictionary.LanguageDictionary;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Stats {
    private final Map<String, Integer> wordUsage;

    private Stats() {
        wordUsage = new ConcurrentHashMap<>();
    }

    public static Stats forDictionary(LanguageDictionary dictionary) {
        var statsForDictionary = new Stats();
        dictionary.getKeys().forEach(key -> statsForDictionary.wordUsage.put(key, 0));
        return statsForDictionary;
    }

    public void updateKeyStats(String word) {
        wordUsage.computeIfPresent(word, (key, count) -> count + 1);
    }

    public Map<String, Integer> get() {
        return Collections.unmodifiableMap(wordUsage);
    }
}
