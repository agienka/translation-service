package com.example.translationservice.stats.api;

import com.example.translationservice.Language;
import com.example.translationservice.stats.Stats;
import com.example.translationservice.stats.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stats")
public class StatsController {

    private final StatsService statsService;

    @Autowired
    public StatsController(StatsService statsService) {
        this.statsService = statsService;
    }

    @GetMapping("/{language}")
    @ResponseBody
    public StatsResponse getStats(@PathVariable("language") String language) {
        Stats keysStats = statsService.getKeysStats(Language.fromString(language));
        return StatsResponse.fromDomain(keysStats);
    }
}
