package com.example.translationservice.stats.api;

import com.example.translationservice.stats.Stats;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class StatsResponse {
    @Getter private final Map<String, Integer> wordStats;

    @JsonCreator
    public StatsResponse(Map<String, Integer> wordStats) {
        this.wordStats = wordStats;
    }

    public static StatsResponse fromDomain(Stats stats) {
        Map<String, Integer> statsSorted = stats.get().entrySet().stream()
            .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (map1, map2) -> map1, LinkedHashMap::new));
        return new StatsResponse(statsSorted);
    }
}
