package com.example.translationservice.dictionary;

import com.example.translationservice.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

@Service
public class DictionaryService {

    private final Map<Language, LanguageDictionary> dictionaries;

    @Autowired
    public DictionaryService(List<LanguageDictionary> dictionaries) {
        this.dictionaries = dictionaries.stream().collect(toMap(LanguageDictionary::getLanguage, Function.identity()));
    }

    public Optional<String> translate(Language language, String word, Function<String, String> formatter) {
        Optional<String> translated = dictionaries.get(language).translate(word);
        return translated.map(formatter);
    }
}
