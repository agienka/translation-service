package com.example.translationservice.dictionary;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toConcurrentMap;
import static java.util.stream.Collectors.toList;

public abstract class ResourceFileLanguageDictionary implements LanguageDictionaryWithStatistics {

    public Map<String, String> load(String fileName) throws IOException {
        var dictionaryFile = new ClassPathResource(fileName).getFile();
        return Files.lines(dictionaryFile.toPath())
                .parallel()
                .map(assureOnlyValidEntries())
                .filter(entry -> entry.size() == 2)
                .collect(toConcurrentMap(lineArr -> lineArr.get(0), lineArr -> lineArr.get(1)));
    }

    @NotNull
    private Function<String, List<String>> assureOnlyValidEntries() {
        return line -> Arrays.stream(line.split("\\|"))
            .map(String::strip)
            .filter(s -> !s.isEmpty() || !s.isBlank())
            .collect(toList());
    }
}
