package com.example.translationservice.dictionary;

import com.example.translationservice.Language;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.EnumMap;

@Configuration
@ConfigurationProperties("dictionary-config")
public class DictionaryConfig {
    @Getter @Setter
    private EnumMap<Language, String> dictionaryFile;
}
