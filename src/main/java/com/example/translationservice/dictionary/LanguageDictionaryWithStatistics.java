package com.example.translationservice.dictionary;


import com.example.translationservice.stats.Stats;

public interface LanguageDictionaryWithStatistics extends LanguageDictionary {
    Stats getStats();
}
