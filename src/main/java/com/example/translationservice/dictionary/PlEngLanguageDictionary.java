package com.example.translationservice.dictionary;

import com.example.translationservice.Language;
import com.example.translationservice.stats.Stats;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.example.translationservice.Language.PLENG;
import static java.util.Optional.ofNullable;

@Component
public class PlEngLanguageDictionary extends ResourceFileLanguageDictionary {

    private final Map<String, String> dictionary;
    private final Stats stats;
    @Getter private final Language language = PLENG;

    public PlEngLanguageDictionary(DictionaryConfig dictionaryConfig) throws IOException {
        dictionary = load(dictionaryConfig.getDictionaryFile().get(language));
        this.stats = Stats.forDictionary(this);
    }

    @Override
    public Optional<String> translate(String word) {
        stats.updateKeyStats(word);
        return ofNullable(dictionary.get(word));
    }

    @Override
    public Stats getStats() {
        return stats;
    }

    @Override
    public Set<String> getKeys() {
        return dictionary.keySet();
    }
}
