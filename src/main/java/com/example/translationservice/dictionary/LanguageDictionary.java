package com.example.translationservice.dictionary;


import com.example.translationservice.Language;

import java.util.Optional;
import java.util.Set;

public interface LanguageDictionary {
    Language getLanguage();
    Set<String> getKeys();
    Optional<String> translate(String word);
}
