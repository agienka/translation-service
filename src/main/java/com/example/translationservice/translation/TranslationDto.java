package com.example.translationservice.translation;

import com.example.translationservice.Language;
import com.example.translationservice.translation.api.TranslationRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class TranslationDto {

    @Getter
    private final Language language;
    @Getter
    private final String content;

    public TranslationDto(Language language, String content) {
        this.language = language;
        this.content = content;
    }

    public static TranslationDto fromRequest(TranslationRequest request) {
        String lang = request.getLanguage() != null ? request.getLanguage().toUpperCase() : "";
        Language language = Language.fromString(lang);
        return new TranslationDto(language, request.getContent());
    }
}
