package com.example.translationservice.translation.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;

public class TranslationResponse {
    @Getter
    private final String content;

    @JsonCreator
    public TranslationResponse(String content) {
        this.content = content;
    }
}
