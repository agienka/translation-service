package com.example.translationservice.translation.api;

import com.example.translationservice.translation.TranslationService;
import com.example.translationservice.translation.formatter.QuotesFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static com.example.translationservice.translation.TranslationDto.fromRequest;

@RestController
@RequestMapping("/translate")
public class TranslationController {

    private final TranslationService translationService;

    @Autowired
    public TranslationController(TranslationService translationService) {
        this.translationService = translationService;
    }

    @PostMapping
    public ResponseEntity<TranslationResponse> translate(@RequestBody TranslationRequest translationRequest,
                                                         @RequestParam(value = "quoted", required = false) boolean quoted) {
        return getTranslation(translationRequest, quoted)
                .map(TranslationResponse::new)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.noContent().build());
    }

    private Optional<String> getTranslation(TranslationRequest translationRequest, boolean quoted) {
        Optional<String> translated;
        if(!quoted) translated = translationService.translate(fromRequest(translationRequest));
        else translated = translationService.translate(fromRequest(translationRequest), QuotesFormatter::format);
        return translated;
    }
}
