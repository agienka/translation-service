package com.example.translationservice.translation.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;

public class TranslationRequest {
    @Getter private final String language;
    @Getter private final String content;

    @JsonCreator
    public TranslationRequest(String language, String content) {
        this.language = language;
        this.content = content;
    }
}
