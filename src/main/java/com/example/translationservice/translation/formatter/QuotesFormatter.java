package com.example.translationservice.translation.formatter;

import java.util.function.Function;

@FunctionalInterface
public interface QuotesFormatter extends Function<String, String> {
    static String format(String word) { return "'" + word + "'"; }
}
