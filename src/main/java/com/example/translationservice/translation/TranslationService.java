package com.example.translationservice.translation;

import com.example.translationservice.dictionary.DictionaryService;
import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

@Service
public class TranslationService {

    public static final String DELIMITER = " ";
    private final DictionaryService dictionaryService;

    @Autowired
    public TranslationService(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    public Optional<String> translate(TranslationDto translation, Function<String, String> formatter) {
        var translated = singleWordsStream(translation)
                .map(word -> dictionaryService.translate(translation.getLanguage(), word, formatter))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(joining(DELIMITER));
        return translated.isBlank() ? Optional.empty() : Optional.of(translated);
    }

    @NotNull
    private Stream<String> singleWordsStream(TranslationDto translation) {
        return Arrays.stream(translation.getContent().split(DELIMITER));
    }

    public Optional<String> translate(TranslationDto translation) {
        return translate(translation, Function.identity());
    }
}
