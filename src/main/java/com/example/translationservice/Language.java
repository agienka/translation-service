package com.example.translationservice;

public enum Language {
    PLENG;

    public static Language fromString(String language) {
        try {
            var languageFormatted = language != null ? language.toUpperCase() : "";
            return Language.valueOf(languageFormatted);
        } catch (IllegalArgumentException ex) {
            throw  new LanguageNotSupportedException(language);
        }
    }
}
