package com.example.translationservice;


import java.util.Arrays;

import static java.util.stream.Collectors.toList;

public class LanguageNotSupportedException extends RuntimeException {

    public LanguageNotSupportedException(String language) {
        super("Language provided: " + language + " is not currently supported. Supported languages: " + Arrays.stream(Language.values()).collect(toList()));
    }
}
