package com.example.translationservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionMapping {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public ExceptionalResponse handleDefault() {
        return new ExceptionalResponse("Something went wrong");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(LanguageNotSupportedException.class)
    public ExceptionalResponse handleLanguageNotSupported(RuntimeException exception) {
        return ExceptionalResponse.fromException(exception);
    }

    public static class ExceptionalResponse {
        private String message;

        @JsonCreator
        public ExceptionalResponse(String message) {
            this.message = message;
        }

        public static ExceptionalResponse fromException(Exception exception) {
            return new ExceptionalResponse(exception.getMessage());
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
