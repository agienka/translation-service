package com.example

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import spock.lang.Specification

class BaseSpec extends Specification {

    @Autowired
    TestRestTemplate restTemplate
}
