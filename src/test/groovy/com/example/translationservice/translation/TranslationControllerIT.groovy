package com.example.translationservice.translation

import com.example.BaseSpec
import com.example.translationservice.ExceptionMapping
import com.example.translationservice.translation.api.TranslationRequest
import com.example.translationservice.translation.api.TranslationResponse
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import spock.lang.Unroll

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TranslationControllerIT extends BaseSpec {

    @Unroll
    def "Should expect to translate #content to #expectedContent"() {
        given:
        def translationRequest = new TranslationRequest(language, content)
        def request = new HttpEntity(translationRequest)
        def queryparams = quoted ? '?quoted=1' : ''

        when:
        def response = restTemplate.exchange("/translate$queryparams", HttpMethod.POST, request, TranslationResponse.class)

        then:
        assert response.statusCode == expectedStatus
        assert response.getBody().content == expectedContent

        where:
        content          | language | quoted | expectedStatus | expectedContent
        'jeden'          | 'pleng'  | false  | HttpStatus.OK  | 'one'
        'jeden dwa pięć' | 'pleng'  | false  | HttpStatus.OK  | 'one two five'
        'jeden dwa brak' | 'pleng'  | false  | HttpStatus.OK  | 'one two'
        'jeden'          | 'pleng'  | true   | HttpStatus.OK  | "'one'"
        'jeden dwa brak' | 'pleng'  | true   | HttpStatus.OK  | "'one' 'two'"
        'jeden dwa pięć' | 'pleng'  | true   | HttpStatus.OK  | "'one' 'two' 'five'"
    }

    @Unroll
    def "Should return NO_CONTENT if no translation available"() {
        given:
        def translationRequest = new TranslationRequest(language, content)
        def request = new HttpEntity(translationRequest)
        def queryparams = quoted ? '?quoted=1' : ''

        when:
        def response = restTemplate.exchange("/translate$queryparams", HttpMethod.POST, request, TranslationResponse.class)

        then:
        assert response.statusCode == expectedStatus
        assert response.getBody() == null

        where:
        content | language | quoted | expectedStatus
        'brak'  | 'pleng'  | false  | HttpStatus.NO_CONTENT
        'brak'  | 'pleng'  | true   | HttpStatus.NO_CONTENT
        ''      | 'pleng'  | false  | HttpStatus.NO_CONTENT
        ''      | 'pleng'  | true   | HttpStatus.NO_CONTENT
    }

    def "Should return BAD_REQUEST if language not supported"() {
        given:
        def translationRequest = new TranslationRequest("notSupported", "to translate")
        def request = new HttpEntity(translationRequest)

        when:
        def response = restTemplate.exchange('/translate', HttpMethod.POST, request, ExceptionMapping.ExceptionalResponse.class)

        then:
        assert response.statusCode == HttpStatus.BAD_REQUEST
    }
}
