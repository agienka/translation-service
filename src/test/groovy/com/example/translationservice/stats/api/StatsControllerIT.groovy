package com.example.translationservice.stats.api

import com.example.BaseSpec
import com.example.translationservice.translation.api.TranslationRequest
import com.example.translationservice.translation.api.TranslationResponse
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import spock.lang.Unroll

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StatsControllerIT extends BaseSpec {

    @Unroll
    def "Should get valid stats"() {
        given:
        def language = 'pleng'
        def translationRequest = new TranslationRequest(language, 'jeden dwa')
        def request = new HttpEntity(translationRequest)

        and:
        timesRequested.times {
            restTemplate.exchange("/translate", HttpMethod.POST, request, TranslationResponse.class)
        }

        when:
        def response = restTemplate.getForEntity("/stats/$language", StatsResponse.class)

        then:
        assert response.statusCode == HttpStatus.OK
        assertSorted(response, timesRequested)

        where:
        timesRequested << 3
    }

    @Unroll
    def "Should get valid stats, when quoted words requested"() {
        given:
        def language = 'pleng'
        def translationRequest = new TranslationRequest(language, 'jeden dwa')
        def request = new HttpEntity(translationRequest)

        and:
        timesRequested.times {
            restTemplate.exchange("/translate?quoted=1", HttpMethod.POST, request, TranslationResponse.class)
        }

        when:
        def response = restTemplate.getForEntity("/stats/$language", StatsResponse.class)

        then:
        assert response.statusCode == HttpStatus.OK
        assertSorted(response, timesRequested)

        where:
        timesRequested << 3
    }

    def "Should return bad request if not supported language provided"() {
        given:
        def language = 'notSupported'

        when:
        def response = restTemplate.getForEntity("/stats/$language", StatsResponse.class)

        then:
        assert response.statusCode == HttpStatus.BAD_REQUEST
    }

    private boolean assertSorted(ResponseEntity<StatsResponse> response, def timesRequested) {
        response.getBody().wordStats.collect { it.key } == ['jeden': timesRequested, 'dwa': timesRequested, 'pięć': 0].collect { it.key }
    }
}
